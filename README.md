# CS371p: Object-Oriented Programming Allocator Repo

* Name1: Jonathan Granier
* Name2: Rithvik Vellaturi

* EID1: jpg2778
* EID2: rkv293

* GitLab ID: jpgranier
* GitLab ID: rith1

* HackerRank ID: rithvikvell

* Git SHA: 431833d1caf815fdb53551719b81e2df8ea9c621

* GitLab Pipelines: https://gitlab.com/jpgranier/cs371p-allocator/-/pipelines

* Estimated completion time: 15

* Actual completion time: 22

* Comments: (any additional comments you have)
