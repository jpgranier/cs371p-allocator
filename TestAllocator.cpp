// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1,);
#else
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);
#endif

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2,);
#else
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);
#endif

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;

    const allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test


TEST(AllocatorFixture3, bad_alloc_1) {
    my_allocator<int, 28> a;
    int successful_allocates = 0;
    try {
        while (true) {
            a.allocate(1);
            ++successful_allocates;
        }
    } catch (std::bad_alloc& ba) {
        ASSERT_EQ(successful_allocates, 1);
        return;
    }
    ASSERT_EQ(true,false);
}

TEST(AllocatorFixture3, good_alloc_1) {
    try {
        my_allocator<int, 12> a;
    } catch (std::bad_alloc& ba) {
        ASSERT_EQ(true,false);
    }
    ASSERT_EQ(true,true);
}

TEST(AllocatorFixture3, bad_alloc_2) {
    try {
        my_allocator<int, 11> a;
    } catch (std::bad_alloc& ba) {
        ASSERT_EQ(true,true);
        return;
    }
    ASSERT_EQ(true,false);
}

TEST(AllocatorFixture3, fully_occupied) {
    my_allocator<int, 36> a;
    a.allocate(1);
    a.allocate(1);
    a.allocate(1);
    my_allocator<int, 36>::const_iterator it = a.const_begin();
    while (it != a.const_end()) {
        ASSERT_EQ(*it, -4);
        ++it;
    }
}

TEST(AllocatorFixture3, fully_occupied_and_cleared) {
    my_allocator<int, 36> a;
    my_allocator<int, 36>::pointer p1 = a.allocate(1);
    my_allocator<int, 36>::pointer p2 = a.allocate(1);
    my_allocator<int, 36>::pointer p3 = a.allocate(1);
    a.deallocate(p1, sizeof(int));
    a.deallocate(p2, sizeof(int));
    a.deallocate(p3, sizeof(int));
    my_allocator<int, 36>::const_iterator it = a.const_begin();
    ASSERT_EQ(*it, 28);
}

TEST(AllocatorFixture3, single_full_allocation) {
    my_allocator<int, 96> a;
    a.allocate(22);
    my_allocator<int, 96>::const_iterator it = a.const_begin();
    ASSERT_EQ(*it, -88);
}

TEST(AllocatorFixture3, single_allocation_and_deallocate) {
    my_allocator<int, 96> a;
    my_allocator<int, 96>::pointer p1 = a.allocate(22);
    a.deallocate(p1, sizeof(int));
    my_allocator<int, 96>::const_iterator it = a.const_begin();
    ASSERT_EQ(*it, 88);
}

TEST(AllocatorFixture3, test_middle_deallocation) {
    my_allocator<int, 36> a;
    a.allocate(1);
    my_allocator<int, 36>::pointer p2 = a.allocate(1);
    a.allocate(1);
    a.deallocate(p2, sizeof(int));
    my_allocator<int, 36>::const_iterator it = a.const_begin();
    ASSERT_EQ(*it, -4);
}

TEST(AllocatorFixture3, test_end_deallocation) {
    my_allocator<int, 36> a;
    a.allocate(1);
    a.allocate(1);
    my_allocator<int, 36>::pointer p3 = a.allocate(1);
    a.deallocate(p3, sizeof(int));
    my_allocator<int, 36>::const_iterator it = a.const_end();
    --it;
    ASSERT_EQ(*it, 4);
}

TEST(AllocatorFixture3, test_invalid_argument) {
    my_allocator<int, 36> a;
    a.allocate(1);
    try {
        a.deallocate(nullptr, sizeof(int));
    } catch (std::invalid_argument) {
        ASSERT_EQ(true,true);
        return;
    }
    ASSERT_EQ(true,false);
}