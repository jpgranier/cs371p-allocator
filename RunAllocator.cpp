// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"
#include <vector>

#define HEAP_SIZE 1000
#define OBJECT_SIZE 8

// ----
// main
// ----

struct Allocated {
    my_allocator<double, HEAP_SIZE>::pointer pointer;
    int size;
};
void swap (Allocated *p, Allocated *z) {
    Allocated temp = *p;
    *p = *z;
    *z = temp;
}

void bubble_sort(std::vector<Allocated>& v) {
    int i, j;
    for (i = 0; i < v.size() - 1; i++) {
        for (j = 0; j < v.size() - i - 1; j++) {
            if (v[j].pointer > v[j+1].pointer) {
                swap(&v[j], &v[j+1]);
            }
        }
    }
}

int main() {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */

    string s;
    bool eof;

    // Get number of tests
    eof = !getline(cin, s);
    assert(!eof);
    int num_cases = stoi(s);
    assert(num_cases > 0);
    assert(num_cases <= 100);

    // Clear blank line
    eof = !getline(cin, s);
    assert(!eof);

    for (int i = 0; i < num_cases; ++i) {
        my_allocator<double, HEAP_SIZE> x;

        std::vector<Allocated> allocated_pointers;
        while (true) {
            eof = !getline(cin, s);
            if (eof || s.empty()) {
                break;
            }

            assert(!eof);
            int task = stoi(s);
            if (task > 0) {
                my_allocator<double, HEAP_SIZE>::pointer p = x.allocate(task);
                Allocated allocated;
                allocated.pointer = p;
                allocated.size  = task;
                allocated_pointers.push_back(allocated);
                my_allocator<double, HEAP_SIZE>::pointer b = p;
                my_allocator<double, HEAP_SIZE>::pointer e = b + task;
                while (p != e) {
                    x.construct(p, 1);
                    ++p;
                }
            } else {
                // deallocate
                bubble_sort(allocated_pointers);
                int index = std::abs(task) - 1;
                Allocated removed = allocated_pointers[index];

                for (int i = 0; i < removed.size; ++i) {
                    x.destroy(removed.pointer + i);
                }

                x.deallocate(removed.pointer, sizeof(double));
                allocated_pointers.erase(allocated_pointers.begin() + index);
            }
        }

        my_allocator<double, HEAP_SIZE>::const_iterator it = x.const_begin();

        while (true) {
            cout << *it;
            ++it;
            if (it == x.const_end()) {
                break;
            }
            cout << " ";
        }
        cout << endl;
    }
    return 0;
}
