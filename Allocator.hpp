// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template<typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator &, const my_allocator &) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator &lhs, const my_allocator &rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using value_type = T;

    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using pointer = value_type *;
    using const_pointer = const value_type *;

    using reference = value_type &;
    using const_reference = const value_type &;

public:
    // ---------------
    // iterator
    // over the blocks meaning if you're on a left sentinel, go to the corresponding right sentinel.
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator &lhs, const iterator &rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator &lhs, const iterator &rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int *_p;
        bool _is_left_sentinel;

    public:
        // -----------
        // constructor
        // -----------

        iterator(int *p) : _p(p) {
            _is_left_sentinel = true;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++() {
            // Iterates from left sentinel to right sentinel alternatively
            int curr_sentinel = std::abs(*_p);
            if (_is_left_sentinel) {
                char* tmp_sentinel = (char*) _p;
                tmp_sentinel += curr_sentinel + sizeof(int);
                _p = (int*) tmp_sentinel;
            } else {
                _p += 1;
            }

            _is_left_sentinel = !_is_left_sentinel;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            // Iterate to the previous right sentinel if currently on left sentinel
            if (_is_left_sentinel) {
                _p -= 1;
            } else {
                // vice versa
                int curr_sentinel = std::abs(*_p);
                char* tmp_curr_sentinel_p = (char*) _p;
                tmp_curr_sentinel_p -= curr_sentinel + sizeof(int);
                _p = (int*)tmp_curr_sentinel_p;

            }
            _is_left_sentinel = !_is_left_sentinel;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator & lhs, const const_iterator & rhs) {
            return lhs._p == rhs._p;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator &lhs, const const_iterator &rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----
        int *_p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator(const int *p) {
            _p = (int*)p;
        }

        // ----------
        // operator *
        // ----------

        const int & operator*() const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator &operator++() {
            // Iterating through every left sentinels
            int curr_sentinel = std::abs(*_p);
            char* tmp_sentinel = (char*) _p;
            tmp_sentinel += curr_sentinel + (2 * sizeof(int));
            _p = (int*) tmp_sentinel;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator++(int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator &operator--() {
            // Iterating through only left sentinels
            int* prev_sentinel_p = _p - 1;
            int prev_sentinel = std::abs(*prev_sentinel_p);
            char* tmp_prev_sentinel_p = (char*) prev_sentinel_p;
            tmp_prev_sentinel_p -= prev_sentinel + sizeof(int);
            _p = (int*) tmp_prev_sentinel_p;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator--(int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Goes through each left sentinel and tallies up total bytes
     * and makes sure it is equal to N
     */
    bool valid() const {
        const_iterator it = const_begin();
        int total = 0;
        // Add up total allocated memory and check that it is equal to the N
        while (it != const_end()) {
            total += std::abs(*it) + (2 * sizeof(int));
            it++;
        }
        return total == N;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator() {
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }
        // Values for what each sentinel should be
        int left_sent = N - (2 * sizeof(int));
        int right_sent = (int) (N - (2 * sizeof(int)));

        // Assigning them to the pointers
        int *left_sent_p = (int *) a;
        *left_sent_p = left_sent;

        int *right_sent_p = (int *) (a + N - sizeof(int));
        *right_sent_p = right_sent;

        assert(valid());
    }

    my_allocator(const my_allocator &) = default;

    ~my_allocator() = default;

    my_allocator &operator=(const my_allocator &) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate(size_type n) {

        iterator it = (int*)a;

        int bytes_allocated = sizeof(T) * n;

        char* break_condition = a + N;
        do {
            int& left_sentinel = *it;
            ++it;
            int& right_sentinel = *it;
            assert(left_sentinel == right_sentinel);
            // Check for the case where the bytes to be allocated is a match for the empty block
            if ((left_sentinel > 0 && (bytes_allocated + (2 * sizeof(int)) == left_sentinel)) || (left_sentinel > 0 && bytes_allocated == left_sentinel)) {
                left_sentinel = -left_sentinel;
                right_sentinel = -right_sentinel;
                int *p = &left_sentinel;
                p += 1;
                return (pointer) p;
            } else if (left_sentinel > 0 && (bytes_allocated + (2 * sizeof(int))) <= left_sentinel) {
                // Location is large enough to allocate memory

                // Step 1: Make current left sentinel the left sentinel of the new block.
                left_sentinel = -bytes_allocated;

                // Step 2: Create a right-most sentinel of the new block
                char* new_tmp_right_sentinel_p = (char *) &left_sentinel;
                new_tmp_right_sentinel_p += bytes_allocated + sizeof(int);
                int *new_right_sentinel_p = (int *) new_tmp_right_sentinel_p;
                *new_right_sentinel_p = -bytes_allocated;

                // Step 3: Add a new left-most block for the shrunken free block.
                int *new_left_sentinel_p = new_right_sentinel_p + 1;
                int shrunken_free_block_size = right_sentinel - bytes_allocated - (2 * sizeof(int));
                *new_left_sentinel_p = shrunken_free_block_size;

                // Step 4: Shrink the original right-most sentinel's size
                right_sentinel = shrunken_free_block_size;

                int* p = &left_sentinel;
                p += 1;
                return (pointer) p;
            } else {
                // Increment to next sentinel
                ++it;
            }
        } while ((char*) &*it < break_condition);

        // If we cannot find space for this block, throw bad_alloc()
        throw std::bad_alloc();
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct(pointer p, const_reference v) {
        new(p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate(pointer arg_p, size_type) {
        if (arg_p == nullptr) {
            throw std::invalid_argument("Pointer is invalid due to nullptr.");
        }

        int *p = (int *) arg_p;
        int *left_sent_p = p - 1;
        // Check if pointer is within bounds of the heap
        if (left_sent_p < (int *) a || left_sent_p > (int *) (a + N - 4)) {
            throw std::invalid_argument("Pointer is out of bounds.");
        }

        iterator it = left_sent_p;

        int& curr_left_sent = *it;
        ++it;
        int& curr_right_sent = *it;
        assert(curr_left_sent == curr_right_sent);
        // Go back to current block left sentinel
        --it;
        assert(curr_left_sent < 0);
        bool is_left_most = left_sent_p == (int*)a;
        bool is_right_most = (((char*) left_sent_p) + std::abs(curr_left_sent) + (2 * sizeof(int))) == (a + N);
        // Block is in the middle
        if (!is_left_most && !is_right_most) {
            ++it; // Current block right sentinel
            ++it; // Next block left sentinel
            ++it; // Next block right sentinel
            int& next_right_sent = *it;
            --it; // Go back to current block left sentinel
            --it;
            --it;
            --it; // Previous block right sentinel
            --it; // Previous block left sentinel
            int& prev_left_sent = *it;

            if (prev_left_sent > 0 && next_right_sent > 0) {
                // Coalesce prev and next
                int free_block_size = prev_left_sent + std::abs(curr_left_sent) + next_right_sent + (4 * sizeof(int));
                prev_left_sent = free_block_size;
                next_right_sent = free_block_size;
            } else if (prev_left_sent > 0) {
                // Coalesce only prev
                int free_block_size = prev_left_sent + std::abs(curr_left_sent) + (2 * sizeof(int));
                prev_left_sent = free_block_size;
                curr_right_sent = free_block_size;

            }  else if (next_right_sent > 0) {
                // Coalesce only next
                int free_block_size = std::abs(curr_left_sent) + next_right_sent + (2 * sizeof(int));
                curr_left_sent = free_block_size;
                next_right_sent = free_block_size;
            } else {
                // No coalescing
                curr_left_sent = -curr_left_sent;
                curr_right_sent = -curr_right_sent;
            }
        }
        else if (!is_left_most) { // Block is right most
            --it;
            --it;
            int& prev_left_sent = *it;
            if (prev_left_sent > 0) {
                // Coalesce only prev
                int free_block_size = prev_left_sent + std::abs(curr_left_sent) + (2 * sizeof(int));
                prev_left_sent = free_block_size;
                curr_right_sent = free_block_size;
            }
            else {
                // No coalescing
                curr_left_sent = -curr_left_sent;
                curr_right_sent = -curr_right_sent;
            }
        }
        else if(!is_right_most) { // Block is left most
            ++it;
            ++it;
            ++it;
            int& next_right_sent = *it;
            if (next_right_sent > 0) {
                // Coalesce only next
                int free_block_size = std::abs(curr_left_sent) + next_right_sent + (2 * sizeof(int));
                curr_left_sent = free_block_size;
                next_right_sent = free_block_size;
            } else {
                // No coalescing
                curr_left_sent = -curr_left_sent;
                curr_right_sent = -curr_right_sent;
            }
        }
        else { // Block is left and right most
            // No coalescing
            curr_left_sent = -curr_left_sent;
            curr_right_sent = -curr_right_sent;
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy(pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int &operator[](int i) {
        return *reinterpret_cast<int *>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int &operator[](int i) const {
        return *reinterpret_cast<const int *>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin() {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator const_begin() const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end() {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator const_end() const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
